#include <math.h>
#include <stdio.h>
#include <erebor_wrapper.h>
#include <zmq.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>

void my_free (void *data, void *hint){
   free (data);
}


char** str_split(char* a_str, const char a_delim){
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

long long current_timestamp() {
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

void make_next_step(float*** prev, float*** process, float* up, float* down, float* left,
                    float* right, float tempon_width){

    for(int i=0; i<tempon_width; i++){
        for(int j=0; j<tempon_width; j++){
            float lup    = i-1 < 0             ? up[j]    : ((float**)*prev)[i-1][j];
            float ldown  = i+1 == tempon_width ? down[j]  : ((float**)*prev)[i+1][j];
            float lleft  = j-1 < 0             ? left[i]  : ((float**)*prev)[i][j-1];
            float lright = j+1 == tempon_width ? right[i] : ((float**)*prev)[i][j+1];
            ((float**)*process)[i][j] = (((float**)*prev)[i][j]+lup+ldown+lleft+lright)/5;
        }
    }
}

int main(int argc, char** argv){
    MPI_Init(&argc, &argv);
    // get my rank
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // get the max rank
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Calcul de la largeur de la grille
    int width = sqrt(size);
    // Calcul du rang du début de la ligne, et celui de la fin de la ligne
    int my_line  = (rank) / width;
    int min_line = (my_line * width);
    int max_line = (my_line * width) + width -1;
    // Calcul des rangs des voisins
    int up    = (((rank - width) % size) + size ) % size;
    int down  = (((rank + width) % size) + size ) % size;
    int left  = rank > min_line ? rank - 1 : max_line;
    int right = rank < max_line ? rank + 1 : min_line;


    // Connexion à Yggdrasil
    int opt;
    char* in_ipc;
    char* out_ipc = malloc(100);
    char* network;
    char* server;
    char* list_ipqueue;
    char** split_ipqueue;
    char* list_portqueue;
    char** split_portqueue;
    char* ipqueue;
    int portqueue;
    int tempon = 100;
    int time;
    int how_many_queus; //TODO split queues information
    int demo=0;
    do{
        opt = getopt (argc, argv, "S:P:N:Q:E:T:Z:W:D");
        switch (opt) {
            case 'S':
                server = optarg;
                break;
            case 'P':
                in_ipc = optarg;
                int iout_ipc = atoi(optarg) + 200 + rank;
                sprintf(out_ipc, "%d", iout_ipc);
                break;
            case 'N':
                network = optarg;
                break;
            case 'W':
                how_many_queus  = atoi(optarg);
                break;
            case 'E':
                list_portqueue = optarg;
                break;
            case 'Q':
                list_ipqueue   = optarg;
                break;
            case 'T' :
                time = atoi(optarg);
                break;
            case 'Z' :
                tempon = atoi(optarg);
                break;
            case 'D':
                demo=1;
                break;
        }
    } while (opt != -1);

    if(demo){
        printf("demo mod activated, no real computations performed\n");
    }else{
        printf("rank %d | size %d, width %d\n", rank, size, width);
        printf("rank %d | my_line %d, min_line %d, max_line %d\n", rank, my_line, min_line, max_line);
        printf("rank %d | up %d, down %d, left %d, right %d\n", rank, up, down, left, right);
    }

    /*
     * Split the list of IPs and PORTs
     */
    split_ipqueue   = str_split(list_ipqueue,   ',');
    split_portqueue = str_split(list_portqueue, ',');
    int my_queue_numbner = rank % how_many_queus;
    ipqueue   = split_ipqueue[my_queue_numbner];
    portqueue = atoi(split_portqueue[my_queue_numbner]);


    char* str_rank = malloc(100);
    sprintf(str_rank, "%d", rank);

    printf("rank %d | %s %s %s %s\n", rank, in_ipc, out_ipc, network, server);

    // Connect to to erebor. Will be useful in the near future.
    erebor* e = malloc(sizeof(erebor));
    int ret = erebor_init_connection(str_rank, network, in_ipc, out_ipc, e);
    if(ret < 0){
        erebor_print_error();
        exit(-2);
    }

    //open a connexion to the queue to send data
    void* context = zmq_ctx_new();
    void* push     = zmq_socket (context, ZMQ_PUSH);

    char push_string[100];
    sprintf (push_string, "tcp://%s:%d", ipqueue,portqueue);
    if (zmq_connect (push, push_string) == -1){
        printf("rank %d | failure connecting push", rank);
        return -1;
    }
    printf("rank %d | %s\n", rank, push_string);
    //for the given simulation time, generate values and send those to the queue
    int buffer_size   = 4 + 4 + tempon*4;

    // Process and allocate my grid.
    int   tempon_width    = sqrt(tempon);
    float** simulation_grid = malloc(sizeof(float*)*tempon_width);
    float** t_1_simulation_grid = malloc(sizeof(float*)*tempon_width);
    for(int i=0; i<tempon_width; i++){
        t_1_simulation_grid[i] = malloc(sizeof(float)*tempon_width);
        simulation_grid[i]     = malloc(sizeof(float)*tempon_width);
        for(int j=0; j<tempon_width; j++){
            simulation_grid[i][j]     = j;
            t_1_simulation_grid[i][j] = j;
        }
    }
    float*** process = &simulation_grid;    // Buffer to store newly processed values
    float*** prev    = &t_1_simulation_grid;// Buffer to send

    printf("rank %d | simulation domain, local %d times %d grid\n", rank, tempon_width, tempon_width);
    // allocate arrays for neighbours data
    MPI_Request req_up;
    MPI_Request req_down;
    MPI_Request req_left;
    MPI_Request req_right;
    float* up_array    = malloc(tempon_width * sizeof(float));
    float* down_array  = malloc(tempon_width * sizeof(float));
    float* left_array  = malloc(tempon_width * sizeof(float));
    float* right_array = malloc(tempon_width * sizeof(float));

    // allocate arrays for me
    MPI_Request send_up;
    MPI_Request send_down;
    MPI_Request send_left;
    MPI_Request send_right;
    float* my_up_array;
    float* my_down_array;
    float* my_left_array  = malloc(tempon_width * sizeof(float));
    float* my_right_array = malloc(tempon_width * sizeof(float));

    printf("rank %d | external arrays allocated \n", rank);
    MPI_Status status;

    MPI_Request barrier_request;

    // until the end
    int nb_sent = 0;
    long long start_time = current_timestamp();
    int keepup = 1;

    // TODO, make the below code timeout aware
    /*
     * If I'm the first rank, send the number of MPI processes to the validator
     */
    if(rank==0){
        char* str_size = malloc(100);
        sprintf(str_size, "size:%d", size);
        erebor_send_to(e, "0", "validator", str_size);
        free(str_size);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // send hello to the validator with my rank
    sprintf(str_rank, "rank:%d", rank);
    erebor_send_to(e, "0", "validator", str_rank);
    free(str_rank);

    //receive the zmq endpoint to send compute resume
    char* src = malloc(200);
    char* group = malloc(200);
    char* zmq_endpoint = malloc(200);
    erebor_recv(e, src, group, zmq_endpoint);
    printf("received endpoint %s", zmq_endpoint);

    //connect the zmq socket to the validator
    void* validator_push  = zmq_socket (context, ZMQ_PUSH);
    if (zmq_connect (validator_push, zmq_endpoint) == -1){
        printf("rank %d | failure connecting validator push", rank);
        return -1;
    }

    // Start computations.
    for (int step=0; keepup; step++){//until the end of timeout
        if(!demo){ // iSend arrays to neighbours
            my_up_array   = ((float**)*prev)[0];
            my_down_array = ((float**)*prev)[tempon_width-1];
            for(int i=0; i<tempon_width; i++){
                my_left_array[i]  = ((float**)*prev)[i][0];
                my_right_array[i] = ((float**)*prev)[i][tempon_width-1];
            }
            MPI_Isend(my_up_array, tempon_width, MPI_FLOAT, up, 0, MPI_COMM_WORLD, &send_up);
            MPI_Isend(my_down_array, tempon_width, MPI_FLOAT, down, 0, MPI_COMM_WORLD, &send_down);
            MPI_Isend(my_left_array, tempon_width, MPI_FLOAT, left, 0, MPI_COMM_WORLD, &send_left);
            MPI_Isend(my_right_array, tempon_width, MPI_FLOAT, right, 0, MPI_COMM_WORLD, &send_right);

            // iReceive arrays from neighbours
            MPI_Irecv(up_array, tempon_width, MPI_FLOAT, up, 0, MPI_COMM_WORLD, &req_up);
            MPI_Irecv(down_array, tempon_width, MPI_FLOAT, down, 0, MPI_COMM_WORLD, &req_down);
            MPI_Irecv(left_array, tempon_width, MPI_FLOAT, left, 0, MPI_COMM_WORLD, &req_left);
            MPI_Irecv(right_array, tempon_width, MPI_FLOAT, right, 0, MPI_COMM_WORLD, &req_right);
        }else{//Ensure all cores going at the same speed
            MPI_Ibarrier(MPI_COMM_WORLD, &barrier_request);
        }


        { // send resume to the validator
            char* resume = malloc(8);
            memcpy(resume,&rank,4);
            memcpy(resume+4,&step,4);
            zmq_msg_t msg;
            assert(zmq_msg_init_data(&msg, resume, 8, my_free, NULL) != -1);
            //try to send the message
            int need_retry = 1;
            while(need_retry && keepup){
                int rc = zmq_msg_send(&msg, validator_push, ZMQ_DONTWAIT);
                if(rc == -1){
                    if(errno == EAGAIN){
                        need_retry = 1;
                    }else{
                        erebor_print_error();
                        exit(-3);
                    }
                }else{
                    need_retry = 0;
                }
                //stop after DURATION seconds
                long long now = current_timestamp();
                keepup = now - start_time < time;
            }
            assert(zmq_msg_close(&msg) != -1);
        }

        {// send data to the message queue
            // Send data to zmq TODO use a custom free function to recylcle buffers.
            char* buffer = malloc(buffer_size);
            memcpy(buffer,&rank,4);
            int position = 4;
            memcpy(buffer+position,&step,4);
            position+=4;
            for(int i=0; i<tempon_width; i++){
                for(int j=0; j<tempon_width; j++){
                    memcpy(buffer+position,&(*prev)[i][j],4);
                    position+=4;
                }
            }
            zmq_msg_t msg;
            assert(zmq_msg_init_data(&msg, buffer, buffer_size, my_free, NULL) != -1);
            //try to send the message
            int need_retry = 1;
            while(need_retry && keepup){
                int rc = zmq_msg_send(&msg, push, ZMQ_DONTWAIT);
                if(rc == -1){
                    if(errno == EAGAIN){
                        need_retry = 1;
                    }else{
                        erebor_print_error();
                        exit(-3);
                    }
                }else{
                    need_retry = 0;
                }
                //stop after DURATION seconds
                long long now = current_timestamp();
                keepup = now - start_time < time;
            }
            assert(zmq_msg_close(&msg) != -1);
            nb_sent++;
        }

        if(keepup){
            if(!demo){//compute new step
                // Wait for the messages from neighbours are available
                int complete = 0;
                int a = 0;
                int b = 0;
                int c = 0;
                int d = 0;
                while(keepup && !complete){
                    if(!a){MPI_Test(&req_up,    &a, &status);}
                    if(!b){MPI_Test(&req_down,  &b, &status);}
                    if(!c){MPI_Test(&req_left,  &c, &status);}
                    if(!d){MPI_Test(&req_right, &d, &status);}
                    complete = a & b & c & d;
                    long long now = current_timestamp();
                    keepup = now - start_time < time;
                }
                if(keepup && complete){
                        // Process simulation
                        make_next_step(prev, process, up_array, down_array, left_array,
                                       right_array, tempon_width);
                }
            }else{//await for the barrier to be complete
                int barrier_test = 0;
                while(!barrier_test && keepup){
                    MPI_Test(&barrier_request, &barrier_test, &status);
                    long long now = current_timestamp();
                    keepup = now - start_time < time;
                }
            }

            // switch processes
            float*** tmp;
            tmp     = prev;
            prev    = process;
            process = tmp;
        }
        long long now = current_timestamp();
        keepup = now - start_time < time;
    }
    erebor_close(e);
    printf("producer end, sent %d\n", nb_sent);
    MPI_Finalize();
}

